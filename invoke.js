'use strict';
exports.__esModule = true;
let _a = require('fabric-network'), FileSystemWallet = _a.FileSystemWallet, Gateway = _a.Gateway;
let fs = require('fs');
let path = require('path');
let redis = require('then-redis');
let ccpPath = path.resolve(__dirname, 'connection.json');
let ccpJSON = fs.readFileSync(ccpPath, 'utf8');
let ccp = JSON.parse(ccpJSON);
let redisClient = redis.createClient();
let promisify = require('util').promisify;
let redisLock = promisify(require('redis-lock')(redisClient));
let logger = require('debug-logger')('optherium-service');
let util = require('util');
//Interface for IO
let readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});

  
//Gets the number of the block and writes it to redis
module.exports.handleBlock = function (block_num, channel) {
    let block = null;
    channel.queryBlock(block_num)
    .then(function (b) {
        block = b;
        return redisClient.get(block_num);
    })
    .then(function (result) {
        if (result == null) {
            let event_block = JSON.stringify(block.data.data[0].payload);
            let eventRaw = ("{\"" + event_block.substring(event_block.indexOf("events"), event_block.indexOf("response\"")));
            let event = (eventRaw.substring(0, eventRaw.length-2)+"}");
            console.log(event);
            if (typeof event !== 'undefined' && event){
                return redisClient.set(event, block_num.toString());
            }
            else
                throw (new Error("Events cannot be find."));
        }
        return result;
    })
    .catch(function (error) {
        logger.error("Unexpected error ocurred: ${error} ");
        throw error;
    });
}

module.exports.handleOldBlocks = function(channel){
    console.log("Handling old blocks...");
    return channel.queryInfo()
    .then(function (info){
        for(let i = info.height.low - 1; i > 0; i--){
            module.exports.handleBlock(i, channel)
        }

        console.log("Old blocks are handled");
    })
    .catch(function (error){
        throw error;
    });
}

module.exports.handleBlockEvent = function (walletPath, username, channelName) {
    let wallet = new FileSystemWallet(walletPath);
    let gateway = new Gateway();
    let client = undefined;
    let channel = undefined;

    wallet.exists(username)
    .then(function (data) {
        if (!data)
            throw (new Error("${username} is not found in a wallet"));
        return gateway.connect(ccp, { wallet: wallet, identity: username, discovery: { enabled: false } });
    })
    .then(function () {
        client = gateway.getClient();
        channel = client.getChannel(channelName);
        module.exports.handleOldBlocks(channel);


        let promises = [];
        var channel_event_hubs = channel.getChannelEventHubsForOrg();
        channel_event_hubs.forEach(function (channel_event_hub) {
            channel_event_hub.registerBlockEvent(function (block, error) {
                if(error){
                    channel.queryInfo().then(info => module.exports.handleBlock(info.height.low - 1, channel));
                }
                else{
                    module.exports.handleBlock(parseInt(block.header.number), channel);
                }
            });
            promises.push(channel_event_hub.connect(true));
        });
        return Promise.all(promises);
    })
    .catch(function (error) {
        logger.error('Error during listener: ${e}');
        throw error;
    });
};
//User inputs for defining wallet username and channel
readline.question('Please enter wallet path: ', function (walletPath) {
    readline.question('Please enter username: ', function (username) {
        readline.question('Please enter the channel: ', function (channel) {
            module.exports.handleBlockEvent(walletPath, username, channel);
        });
    });
});
