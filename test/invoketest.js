const { handleBlock, handleBlockEvent } = require('./../invoke.js');
const { describe, it} = require('mocha');
const fs = require('fs');
const path = require('path');
const assert = require('assert');
const redis = require('redis');
const client = redis.createClient();
let { FileSystemWallet, Gateway, Global } = require('fabric-network');
const ccpPath = path.resolve(__dirname,'..', 'connection.json');
const ccpJSON = fs.readFileSync(ccpPath, 'utf8');
const ccp = JSON.parse(ccpJSON);
let redisClient = redis.createClient();
const expect = require('chai').expect;

let numberOfBlocksInLedger = function(num){ 
    let wallet = new FileSystemWallet('./../wallet');
    let gateway = new Gateway();


    wallet.exists('admin')
    .then(function () {
        return gateway.connect(ccp, { wallet, identity: 'admin', discovery: { enabled: false } });
    })
    .then(function () {
        let client = gateway.getClient();
        let channel = client.getChannel('mychannel');
        channel.queryInfo().then(b=>{
            expect(b.height.low-1).to.equal(num);

        });
    })
    .catch(function (e){
        console.log(e);
    });
    
}

describe('Redis should contain the same amount of blocks as in ledger', function() {
    it('redis', function() {
        handleBlockEvent('../wallet', 'admin', 'mychannel');
        client.keys('*', function (err, keys){
            if(err) return;
            
            numberOfBlocksInLedger(keys.length);
        });
       
    });
});


describe('Check for replication when scaling', function() {
    it('redis', function() {
        let numberOfKeys = 0;


        handleBlockEvent('../wallet', 'admin', 'mychannel');
        handleBlockEvent('../wallet', 'admin', 'mychannel');
        handleBlockEvent('../wallet', 'admin', 'mychannel');
        client.keys('*', function (err, keys){
            if(err) return;
            
            numberOfBlocksInLedger(keys.length);
        });
       
    });
});

describe('Check if last block is in redis', function() {
    it('redis', function() {
        let wallet = new FileSystemWallet('./../wallet');
        let gateway = new Gateway();
    
    
        wallet.exists('admin')
        .then(function () {
            return gateway.connect(ccp, { wallet, identity: 'admin', discovery: { enabled: false } });
        })
        .then(function () {
            let client = gateway.getClient();
            let channel = client.getChannel('mychannel');
            return channel.queryInfo()
        })
        .then(function (info){
            let client = gateway.getClient();
            let channel = client.getChannel('mychannel');
            return channel.queryBlock(info.height.low-1);
        })
        .then(function (block){
            return redisClient.get("20", (res, err)=>{
                console.log(res+" " + err);
                //expect(err).to.equal(null);
                if(res != null){
                    assert.equal(false, false);
                }

            });
        })
        .catch(function (e){
            console.log(e);
        });
       
    });
});