'use strict';

import { Channel } from "fabric-client";

let { FileSystemWallet, Gateway } = require('fabric-network');
const fs = require('fs');
const path = require('path');
const redis = require('redis');
const ccpPath = path.resolve(__dirname, 'connection.json');
const ccpJSON = fs.readFileSync(ccpPath, 'utf8');
const ccp = JSON.parse(ccpJSON);
const redisClient = redis.createClient();
const {promisify} = require('util');
const redisLock = promisify(require('redis-lock')(redisClient));
const logger = require('debug-logger')('optherium-service');

//Interface for IO
let readline = require('readline').createInterface( {
    input: process.stdin,
    output: process.stdout
});

//Gets the number of the block and writes it to redis
module.exports.handleBlock =(block_num: number, channel: Channel)=> { 
    let block = null;
    channel.queryBlock(block_num)
    .then(function (b) {
        block = b;
        return redisClient.get(block_num);
    })
    .then(function (result) {
        if(result == null){
            if(typeof block.data !== 'undefined' && typeof block.data.data[0] !== 'undefined' && block.data.data[0].payload !== 'undefined' && block.data)
                return redisClient.set(block.data.data[0].payload.header.channel_header.tx_id.toString(), block_num.toString());
            else
                throw (new Error("txID cannot be find."));
        }
        return result;
    })
    .catch(function (error) {
        logger.error("Unexpected error ocurred: ${error} ");
        throw error;
    });
}

module.exports.handleBlockEvent = function (walletPath: string,username:string , channelName:string) {
    let wallet = new FileSystemWallet(walletPath);
    let gateway = new Gateway();


    wallet.exists(username)
    .then(function (data: boolean) {
        if(!data)
            throw (new Error("Username is not found in a wallet"));
        return gateway.connect(ccp, { wallet, identity: username, discovery: { enabled: false } });
    })
    .then(function () {
        let client = gateway.getClient();
        let channel = client.getChannel(channelName);
        let channel_event_hubs = channel.getChannelEventHubsForOrg();
        let promises = [];
        channel_event_hubs.forEach((channel_event_hub) => {
            channel_event_hub.registerBlockEvent((block) => {                
                for(let i = parseInt(block.header.number); i > 0; i--) {
                    module.exports.handleBlock(i, channel);        
                }
            });
            promises.push(channel_event_hub.connect(true));
        });
        return Promise.all(promises);
    
    })
    .then(function () {
        console.log("Redis lock is on");
        return(redisLock('hashWritingLock'));
    })
    .catch(function (error: Error) {
            logger.error('Error during listener: ${e}');
            throw error;
    });
}


//User inputs for defining wallet username and channel
readline.question('Please enter wallet path: ', (walletPath: string)=> {
    readline.question('Please enter username: ', (username: string)=> {
        readline.question('Please enter the channel: ', (channel: string)=> {
            module.exports.handleBlockEvent(walletPath, username, channel);
        });
    });
});
